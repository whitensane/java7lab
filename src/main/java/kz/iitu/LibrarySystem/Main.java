package kz.iitu.LibrarySystem;

import kz.iitu.LibrarySystem.config.SpringConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        RunApp library = context.getBean("runApp", RunApp.class);
        library.runApp();
    }
}