package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.User;
import kz.iitu.LibrarySystem.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Transactional
    public User getUser(String name, String surname) {
        User user = userRepository.findUserByNameAndSurname(name, surname);
        return user;
    }
}

