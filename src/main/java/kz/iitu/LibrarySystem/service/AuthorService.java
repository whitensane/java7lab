package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    @Transactional
    public void createAuthor(Author author) {
        authorRepository.save(author);
    }

    @Transactional
    public List getAllAuthors() {
        return (List<Author>) authorRepository.findAll();
    }

    @Transactional
    public Author getAuthor(Long id) {
        Author author = authorRepository.findAuthorById(id);
        return author;
    }

    @Transactional
    public Author getAuthor(String name, String surname) {
        Author author = authorRepository.findAuthorByNameAndSurname(name,surname);
        return author;
    }
}
