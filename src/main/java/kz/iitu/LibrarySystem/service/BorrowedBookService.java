package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.BorrowedBook;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.entity.User;
import kz.iitu.LibrarySystem.notification.BorrowedBookNotifyReturnDateEvent;
import kz.iitu.LibrarySystem.repository.BorrowedBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class BorrowedBookService implements ApplicationEventPublisherAware {
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private BorrowedBookRepository borrowedBookRepository;

    @Transactional
    public void createBook(BorrowedBook book) {
        borrowedBookRepository.save(book);
        this.eventPublisher.publishEvent(new BorrowedBookNotifyReturnDateEvent(this, book.getId(),book.getIssue_date(), book.getReturn_date()));
        System.out.println("ISSUE DATE: " + book.getIssue_date());
        System.out.println("RETURN DATE: " + book.getReturn_date());
    }

    @Transactional
    public List getAllBorrowedBooks(User user) {
        return (List<BorrowedBook>) borrowedBookRepository.findBookByUser(user);
    }
    @Transactional
    public List showBookByStatus(Status status) {
        return (List<BorrowedBook>) borrowedBookRepository.findAllByStatus(status);
    }

    @Transactional
    public BorrowedBook getBookById(Long id) {
        return borrowedBookRepository.findBookById(id);
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }

    @Transactional
    public void updateBook(Long id, Status status) {
        BorrowedBook updateBook = borrowedBookRepository.findBookById(id);
        updateBook.setStatus(status);
        borrowedBookRepository.save(updateBook);
        System.out.println("Book was updated!");
    }
}
