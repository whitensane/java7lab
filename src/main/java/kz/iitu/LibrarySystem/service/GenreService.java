package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Genre;
import kz.iitu.LibrarySystem.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class GenreService {
    @Autowired
    private GenreRepository genreRepository;

    @Transactional
    public void createGenre(Genre genre) {
        genreRepository.save(genre);
    }

    @Transactional
    public List getAllGenres() {
        return (List<Genre>) genreRepository.findAll();
    }
}
