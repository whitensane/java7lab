package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    @Transactional
    public void createBook(Book book) {
        bookRepository.save(book);
    }

    @Transactional
    public List getAllBooks() {
        return (List<Book>) bookRepository.findAll();
    }

    @Transactional
    public List showBookByStatus(Status status) {
        return (List<Book>) bookRepository.findAllByStatus(status);
    }

    @Transactional
    public List findBookByAuthor(Author author) {
        return (List<Book>) bookRepository.findBookByAuthor(author);
    }

    @Transactional
    public Book showBookByDescription(String description) {
        return bookRepository.findBookByDescription(description);
    }

    @Transactional
    public Book showBookByName(String name) {
        return bookRepository.findBookByName(name);
    }

    @Transactional
    public Book showBookById(Long id) {
        return bookRepository.findBookById(id);
    }

    @Transactional
    public void updateBook(String isbn,int quantity) {
        Book updateBook = bookRepository.findBookByIsbn(isbn);
        updateBook.setQuantity(quantity);
        bookRepository.save(updateBook);
        System.out.println("Book was updated!");
    }

    @Transactional
    public void updateBookStatus(String isbn,Status status) {
        Book updateBook = bookRepository.findBookByIsbn(isbn);
        updateBook.setStatus(status);
        bookRepository.save(updateBook);
        System.out.println("Book was updated!");
    }

    @Transactional
    public Book showBookByIsbn(String isbn) {
        return bookRepository.findBookByIsbn(isbn);
    }
}

