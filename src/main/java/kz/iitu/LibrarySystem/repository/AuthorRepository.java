package kz.iitu.LibrarySystem.repository;

import kz.iitu.LibrarySystem.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    Author findAuthorById(Long id);

    Author findAuthorByNameAndSurname(String name, String surname);
}