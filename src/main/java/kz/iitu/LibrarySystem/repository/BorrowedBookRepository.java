package kz.iitu.LibrarySystem.repository;

import kz.iitu.LibrarySystem.entity.BorrowedBook;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BorrowedBookRepository extends JpaRepository<BorrowedBook, Long> {
    List<BorrowedBook> findBookByUser(User user);

    BorrowedBook findBookById(Long id);

    List<BorrowedBook> findAllByStatus(Status status);
}