package kz.iitu.LibrarySystem.repository;

import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    Book findBookByName(String name);

    Book findBookById(Long id);

    List<Book> findBookByAuthor(Author author);

    Book findBookByDescription(String Description);

    List<Book> findAllByStatus(Status status);

    Book findBookByIsbn(String isbn);

}