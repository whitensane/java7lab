package kz.iitu.LibrarySystem.repository;

import kz.iitu.LibrarySystem.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByNameAndSurname(String name, String surname);
}