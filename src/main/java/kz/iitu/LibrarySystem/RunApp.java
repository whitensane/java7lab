package kz.iitu.LibrarySystem;

import kz.iitu.LibrarySystem.controller.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class RunApp {
    Scanner in = new Scanner(System.in);

    @Autowired
    private BookController bookController;

    @Autowired
    private AuthorController authorController;

    @Autowired
    private GenreController genreController;

    @Autowired
    private UserController userController;

    @Autowired
    private BorrowedBookController borrowedBookController;

    public void runApp() {
        int choice = 0;
        while (choice != 9) {
            System.out.println(" Choose the option by using number! ");
            System.out.println(" 1.Create an author ");
            System.out.println(" 2.Create a book ");
            System.out.println(" 3.Create a genre");
            System.out.println(" 4.Create a user ");
            System.out.println(" 5.Borrow a book ");
            System.out.println(" 6.Return a book ");
            System.out.println(" 7.Search books ");
            System.out.println(" 8.Show books ");
            System.out.println(" 9.Exit ");
            choice = in.nextInt();

            switch (choice) {
                case 1:
                    authorController.createAuthor();
                    break;
                case 2:
                    bookController.createBook();
                    break;
                case 3:
                    genreController.createGenre();
                    break;
                case 4:
                    userController.createUser();
                    break;
                case 5:
                    borrowedBookController.createRequest();
                    break;
                case 6:
                    borrowedBookController.returnBook();
                    break;
                case 7:
                    bookController.search();
                    break;
                case 8:
                    bookController.show();
                    break;
                case 9:
                    System.out.println(" Exit ");
                    System.exit(0);
            }
        }
    }
}