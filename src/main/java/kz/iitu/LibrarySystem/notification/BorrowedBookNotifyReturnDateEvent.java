package kz.iitu.LibrarySystem.notification;

import kz.iitu.LibrarySystem.controller.BorrowedBookController;
import org.springframework.context.ApplicationEvent;

import java.util.Date;

public class BorrowedBookNotifyReturnDateEvent extends ApplicationEvent {
    private Long id;
    private Date issue_date;
    private Date return_date;
    private BorrowedBookController borrowedBookController;

    public BorrowedBookNotifyReturnDateEvent(Object source, Long id, Date issue_date, Date return_date) {
        super(source);
        this.id = id;
        this.issue_date = issue_date;
        this.return_date = return_date;
    }

    public void notifyDate() {
        borrowedBookController.createRequest();
    }
}