package kz.iitu.LibrarySystem.notification.handler;

import kz.iitu.LibrarySystem.notification.BorrowedBookNotifyReturnDateEvent;
import org.springframework.context.ApplicationListener;

public class BorrowedBookNotifyReturnDateHandler implements ApplicationListener<BorrowedBookNotifyReturnDateEvent> {

    @Override
    public void onApplicationEvent(BorrowedBookNotifyReturnDateEvent borrowedBookNotifyReturnDateEvent) {
        borrowedBookNotifyReturnDateEvent.notifyDate();
    }
}