package kz.iitu.LibrarySystem.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(unique = true)
    private String isbn;
    private String description;
    private int quantity;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id", updatable = false)
    private Author author;
    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Genre> genres;
    @Enumerated(EnumType.STRING)
    private Status status;
    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    private Set<BorrowedBook> borrowedBooks;

    public Book(String name, String isbn, String description, int quantity, Status status) {
        this.name = name;
        this.isbn = isbn;
        this.description = description;
        this.quantity = quantity;
        this.status = status;
    }

    public Book(String name, String isbn, String description, int quantity, Author author, Status status) {
        this.name = name;
        this.isbn = isbn;
        this.description = description;
        this.quantity = quantity;
        this.author = author;
        this.status = status;
    }

    public Book() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<BorrowedBook> getBorrowedBooks() {
        return borrowedBooks;
    }

    public void setBorrowedBooks(Set<BorrowedBook> borrowedBooks) {
        this.borrowedBooks = borrowedBooks;
    }

    @Override
    public String toString() {
        return "\nBook_ID: " + id +
                "\nName: " + name +
                "\nISBN: " + isbn +
                "\nDescription: " + description +
                "\nQuantity: " + quantity +
                "\nGenres: " + genres +
                "\nStatus: " + status;
    }
}