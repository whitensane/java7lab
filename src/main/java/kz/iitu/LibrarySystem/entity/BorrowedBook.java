package kz.iitu.LibrarySystem.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "borrowedBooks")
public class BorrowedBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "book_id", updatable = false)
    private Book book;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", updatable = false)
    private User user;
    private Date issue_date;
    private Date return_date;
    private Status status;

    public BorrowedBook(Book book, User user, Date issue_date, Date return_date, Status status) {
        this.book = book;
        this.user = user;
        this.issue_date = issue_date;
        this.return_date = return_date;
        this.status = status;
    }

    public BorrowedBook() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(Date issue_date) {
        this.issue_date = issue_date;
    }

    public Date getReturn_date() {
        return return_date;
    }

    public void setReturn_date(Date return_date) {
        this.return_date = return_date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "\nBorrowedBook_ID: " + id +
                "\nIssue_date: " + issue_date +
                "\nReturn_date: " + return_date +
                "\nStatus: " + status;
    }
}