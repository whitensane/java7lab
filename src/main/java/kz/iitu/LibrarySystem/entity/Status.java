package kz.iitu.LibrarySystem.entity;

public enum Status {
    AVAILABLE,
    OVERDUE,
    NOT_AVAILABLE,
    ISSUED,
    REQUESTED,
}