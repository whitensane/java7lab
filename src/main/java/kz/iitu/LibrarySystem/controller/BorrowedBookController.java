package kz.iitu.LibrarySystem.controller;

import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.BorrowedBook;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.entity.User;
import kz.iitu.LibrarySystem.service.BookService;
import kz.iitu.LibrarySystem.service.BorrowedBookService;
import kz.iitu.LibrarySystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

@Controller
public class BorrowedBookController {
    Scanner in = new Scanner(System.in);
    public static final String DATE_FORMAT = "dd-MM-yyyy";

    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    @Autowired
    private BorrowedBookService borrowedBookService;

    public void createRequest() {
        System.out.println("Enter name of user:");
        String userName = in.next();
        System.out.println("Enter surname of user:");
        String userSurname = in.next();
        User user = userService.getUser(userName, userSurname);
        System.out.println(bookService.getAllBooks());
        System.out.println("Enter ID of book:");
        Long book_id = in.nextLong();
        Book book = bookService.showBookById(book_id);
        BorrowedBook borrowedBook = null;
        switch (book.getStatus()) {
            case AVAILABLE:
                System.out.println("DATE_FORMAT = dd-MM-yyyy");
                switch (book.getQuantity()) {
                    case 0:
                        bookService.updateBookStatus(book.getIsbn(), Status.NOT_AVAILABLE);
                        System.out.println("You cannot borrow this book!");
                        borrowedBook = new BorrowedBook(book, user, null, null, Status.REQUESTED);
                        borrowedBookService.createBook(borrowedBook);
                        break;
                    default:
                        System.out.println("Enter issue date:");
                        String issue_date = new Scanner(System.in).nextLine();
                        Date issue = null;
                        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                        try {
                            issue = dateFormat.parse(issue_date);
                            System.out.println(issue.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Enter return date:");
                        String return_date = new Scanner(System.in).nextLine();
                        Date return_d = null;
                        try {
                            return_d = dateFormat.parse(return_date);
                            System.out.println(return_d.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        borrowedBook = new BorrowedBook(book, user, issue, return_d, Status.ISSUED);
                        borrowedBookService.createBook(borrowedBook);
                        bookService.updateBook(borrowedBook.getBook().getIsbn(), book.getQuantity() - 1);
                        System.out.println("You borrowed a book!");
                        if (book.getQuantity() == 0) {
                            bookService.updateBookStatus(book.getIsbn(), Status.NOT_AVAILABLE);
                        }
                        break;
                }
                break;
            case NOT_AVAILABLE:
                System.out.println("You cannot borrow this book! Status of book: " + book.getStatus());
                borrowedBook = new BorrowedBook(book, user, null, null, Status.REQUESTED);
                borrowedBookService.createBook(borrowedBook);
                bookService.updateBook(borrowedBook.getBook().getIsbn(), book.getQuantity());
                break;
        }
        if (book.getQuantity() == 0) {
            bookService.updateBookStatus(book.getIsbn(), Status.NOT_AVAILABLE);
        }
    }

    public void returnBook() {
        System.out.println("Enter name of user:");
        String userName = in.next();
        System.out.println("Enter surname of user:");
        String userSurname = in.next();
        User user = userService.getUser(userName, userSurname);
        System.out.println(borrowedBookService.getAllBorrowedBooks(user));
        System.out.println("Enter ID of BORROWED book:");
        Long id = in.nextLong();
        BorrowedBook borrowedBook = borrowedBookService.getBookById(id);
        Date date = new Date();
        if (borrowedBook.getReturn_date().compareTo(date) < 0) {
            bookService.updateBook(borrowedBook.getBook().getIsbn(), borrowedBook.getBook().getQuantity() + 1);
            borrowedBookService.updateBook(id, Status.OVERDUE);
        } else {
            bookService.updateBook(borrowedBook.getBook().getIsbn(), borrowedBook.getBook().getQuantity() + 1);
        }
        System.out.println(borrowedBookService.getBookById(id));

    }
}