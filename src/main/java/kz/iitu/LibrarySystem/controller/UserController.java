package kz.iitu.LibrarySystem.controller;

import kz.iitu.LibrarySystem.entity.User;
import kz.iitu.LibrarySystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Scanner;

@Controller
public class UserController {
    Scanner in = new Scanner(System.in);

    @Autowired
    private UserService userService;

    public void createUser() {
        System.out.println("Enter name of user:");
        String name = in.next();
        System.out.println("Enter surname of user:");
        String surname = in.next();
        System.out.println("Enter phone number of user:");
        int phoneNumber = in.nextInt();
        User user = new User(name, surname, phoneNumber);
        userService.createUser(user);
        System.out.println("You created a user!");
    }
}