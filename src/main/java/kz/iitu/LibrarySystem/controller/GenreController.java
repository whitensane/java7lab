package kz.iitu.LibrarySystem.controller;
import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.Genre;
import kz.iitu.LibrarySystem.service.BookService;
import kz.iitu.LibrarySystem.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.Scanner;

@Controller
public class GenreController {
    Scanner in = new Scanner(System.in);

    @Autowired
    private GenreService genreService;

    @Autowired
    private BookService bookService;

    public void createGenre(){
        System.out.println("Enter name of genre:");
        String name = in.next();
        System.out.println("Enter description of genre:");
        String description = in.next();
        System.out.println(bookService.getAllBooks());
        System.out.println("Enter ID of book:");
        Long book_id = in.nextLong();
        Book book = bookService.showBookById(book_id);
        Genre genre = new Genre(name, description,book);
        genreService.createGenre(genre);
        System.out.println("You created a genre!");
    }
}