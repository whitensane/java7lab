package kz.iitu.LibrarySystem.controller;

import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Scanner;

@Controller
public class AuthorController {
    Scanner in = new Scanner(System.in);

    @Autowired
    private AuthorService authorService;

    public void createAuthor() {
        System.out.println("Enter name of author:");
        String name = in.next();
        System.out.println("Enter surname of author:");
        String surname = in.next();
        System.out.println("Enter biography of author:");
        String biography = in.next();
        Author author = new Author(name, surname, biography);
        authorService.createAuthor(author);
        System.out.println("You created an author!");
    }
}