package kz.iitu.LibrarySystem.controller;
import kz.iitu.LibrarySystem.entity.*;
import kz.iitu.LibrarySystem.service.AuthorService;
import kz.iitu.LibrarySystem.service.BookService;
import kz.iitu.LibrarySystem.service.BorrowedBookService;
import kz.iitu.LibrarySystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.Scanner;

@Controller
public class BookController {
    Scanner in = new Scanner(System.in);

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private BorrowedBookService borrowedBookService;

    public void createBook(){
        System.out.println("Enter name of book:");
        String name = in.next();
        System.out.println("Enter ISBN of book:");
        String isbn = in.next();
        System.out.println("Enter description of book:");
        String description = in.next();
        System.out.println("Enter quantity of book:");
        int quantity = in.nextInt();
        System.out.println( authorService.getAllAuthors());
        System.out.println("Enter ID of author:");
        Long author_id = in.nextLong();
        Author author = authorService.getAuthor(author_id);
        System.out.println("Enter status of book: ");
        System.out.println("AVAILABLE");
        System.out.println("NOT_AVAILABLE");
        int statusType = in.nextInt();
        Status status_type = null;
        switch (statusType) {
            case 1:
                status_type = Status.AVAILABLE;
                break;
            case 2:
                status_type = Status.NOT_AVAILABLE;
                break;
        }

        Book book = new Book(name,isbn,description,quantity,author,status_type);
        bookService.createBook(book);
        System.out.println("You created a book!");
    }

    public void show() {
        int choice = 0;
        while (choice != 6) {
            System.out.println(" Choose the option by using number! Show books");
            System.out.println(" 1.ISSUED");
            System.out.println(" 2.REQUESTED");
            System.out.println(" 3.OVERDUE");
            System.out.println(" 4.AVAILABLE");
            System.out.println(" 5.NOT AVAILABLE");
            System.out.println(" 6.Exit ");
            choice = in.nextInt();

            switch (choice) {
                case 1:
                    System.out.println(bookService.showBookByStatus(Status.ISSUED));
                    System.out.println(borrowedBookService.showBookByStatus(Status.ISSUED));
                    break;
                case 2:
                    System.out.println(borrowedBookService.showBookByStatus(Status.REQUESTED));
                    break;
                case 3:
                    System.out.println(borrowedBookService.showBookByStatus(Status.OVERDUE));
                    break;
                case 4:
                    System.out.println(bookService.showBookByStatus(Status.AVAILABLE));
                    break;
                case 5:
                    System.out.println(bookService.showBookByStatus(Status.NOT_AVAILABLE));
                    break;
                case 6:
                    System.out.println(" Exit ");
                    return;
            }
        }
    }

    public void search(){
        int choice = 0;
        while (choice != 5) {
            System.out.println(" Choose the option by using number! Search books");
            System.out.println(" 1.By name");
            System.out.println(" 2.By description");
            System.out.println(" 3.By author");
            System.out.println(" 4.Requested by member ");
            System.out.println(" 5.Exit ");
            choice = in.nextInt();

            switch (choice) {
                case 1:
                    System.out.println("Enter name of book:");
                    String name = in.next();
                    System.out.println(bookService.showBookByName(name));
                    break;
                case 2:
                    System.out.println("Enter description of book:");
                    String description = in.next();
                    System.out.println(bookService.showBookByDescription(description));

                    break;
                case 3:
                    System.out.println("Enter name of author:");
                    String author_name = in.next();
                    System.out.println("Enter surname of author:");
                    String author_surname = in.next();
                    Author author = authorService.getAuthor(author_name,author_surname);
                    System.out.println(bookService.findBookByAuthor(author));
                    break;
                case 4:
                    System.out.println("Enter name of user:");
                    String userName = in.next();
                    System.out.println("Enter surname of user:");
                    String surname = in.next();
                    User user = userService.getUser(userName,surname);
                    System.out.println(borrowedBookService.getAllBorrowedBooks(user));
                    break;
                case 5:
                    System.out.println(" Exit ");
                    return;
            }
        }
    }
}